const { Client, MessageMedia, LocalAuth, Buttons, List } = require('whatsapp-web.js/index');
// const http = require('node:http');
const express = require('express');
const socketIO = require('socket.io');
const qrcode = require('qrcode');
const bodyParser = require('body-parser');
const http = require('http');
const https = require('https');
const fs = require('fs');
const { phoneNumberFormatter } = require('./helpers/formatter');
const fileUpload = require('express-fileupload');
const axios = require('axios');
const { log } = require('console');
const port = process.env.PORT || 8181;

const app = express();

const server = http.createServer(app);
const io = socketIO(server);

app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

/**
 * BASED ON MANY QUESTIONS
 * Actually ready mentioned on the tutorials
 * 
 * The two middlewares above only handle for data json & urlencode (x-www-form-urlencoded)
 * So, we need to add extra middleware to handle form-data
 * Here we can use express-fileupload
 */
app.use(fileUpload({
  debug: false
}));

app.get('/', (req, res) => {
  res.sendFile('home.html', {
    root: __dirname
  });
});
app.use(express.static(__dirname + '/'));

app.get('/device', (req, res) => {
  res.sendFile('device.html', {
    root: __dirname
  });
});
app.get('/dev', (req, res) => {
  res.sendFile('index-multiple-account.html', {
    root: __dirname
  });
});
const sessions = [];
const SESSIONS_FILE = './whatsapp-sessions.json';

const createSessionsFileIfNotExists = function () {
  if (!fs.existsSync(SESSIONS_FILE)) {
    try {
      fs.writeFileSync(SESSIONS_FILE, JSON.stringify([]));
      console.log('Sessions file created successfully.');
    } catch (err) {
      console.log('Failed to create sessions file: ', err);
    }
  }
}

createSessionsFileIfNotExists();

const setSessionsFile = function (sessions) {
  fs.writeFile(SESSIONS_FILE, JSON.stringify(sessions), function (err) {
    if (err) {
      console.log(err);
    }
  });
}

const getSessionsFile = function () {
  return JSON.parse(fs.readFileSync(SESSIONS_FILE));
}

const removeSession = function (key) {
  console.log('Remove session: ' + key);
  const device = sessions.find(sess => sess.id == key)?.device;
  // device.getState().then((result) => { 
  //   if (result == 'CONNECTED') { 
  //     device.logout();
  //     io.emit('message', { apikey: key, text: 'Whatsapp logout!' });
  //   }
  // })
  io.emit('message', { apikey: key, text: 'Whatsapp is disconnected!' });
  device.destroy();
  device.initialize();

  // Menghapus pada file sessions
  const savedSessions = getSessionsFile();
  const sessionIndex = savedSessions.findIndex(sess => sess.id == key);
  savedSessions.splice(sessionIndex, 1);
  // console.log(savedSessions);
  setSessionsFile(savedSessions);
  io.emit('remove-session', key);
}

const logoutSession = function (key) {
  console.log('Logout session: ' + key);
  const device = sessions.find(sess => sess.id == key)?.device;
  io.emit('message', { apikey: key, text: 'Whatsapp logout!' });
  device.logout();
  // Menghapus pada file sessions
  const savedSessions = getSessionsFile();
  const sessionIndex = savedSessions.findIndex(sess => sess.id == key);
  savedSessions[sessionIndex].ready = false;
  setSessionsFile(savedSessions);
}

const createSession = function (nama, api, description, key) {
  const device = new Client({
    restartOnAuthFail: true,
    // takeoverOnConflict: true,
    puppeteer: {
      headless: true,
      args: [
        '--no-sandbox',
        '--disable-setuid-sandbox',
        '--disable-dev-shm-usage',
        '--disable-accelerated-2d-canvas',
        '--no-first-run',
        '--no-zygote',
        '--single-process', // <- this one doesn't works in Windows
        '--disable-gpu'
      ],
    },
    authStrategy: new LocalAuth({
      clientId: key
    })
  });

  device.initialize();

  device.on('qr', (qr) => {
    console.log('QR RECEIVED', qr);
    qrcode.toDataURL(qr, (err, url) => {
      io.emit('qr', { apikey: key, src: url });
      io.emit('message', { apikey: key, text: 'Please scan!' });
    });
  });

  device.on('message', (message) => {
    const headers = {
      'Content-Type': 'application/json',
      'apikey': key,
    };
    axios.post(api, message, { headers })
      .then(response => {
      })
      .catch(error => {
      });
  });

  device.on('change_state', state => {
    console.log('CHANGE STATE', state);
  });

  device.on('ready', () => {
    io.emit('ready', { apikey: key });
    io.emit('message', { apikey: key, text: 'Whatsapp ready!' });

    const savedSessions = getSessionsFile();
    const sessionIndex = savedSessions.findIndex(sess => sess.id == key);
    savedSessions[sessionIndex].ready = true;
    setSessionsFile(savedSessions);
  });

  device.on('authenticated', () => {
    io.emit('authenticated', { apikey: key });
    io.emit('message', { apikey: key, text: 'Whatsapp authenticated!' });
  });
  device.on('change_state', async (newState) => {
    console.log(newState);
  })
  device.on('auth_failure', function () {
    io.emit('message', { apikey: key, text: 'Auth failure, restarting...' });
  });

  device.on('disconnected', (reason) => {
    io.emit('message', { apikey: key, text: 'Whatsapp disconnected!' });
    device.destroy();
    device.initialize();
    // Menghapus pada file sessions
    const savedSessions = getSessionsFile();
    const sessionIndex = savedSessions.findIndex(sess => sess.id == key);
    savedSessions[sessionIndex].ready = false;

    // io.emit('remove-session', key);
  });

  // Tambahkan device ke sessions
  sessions.push({
    id: key,
    nama: nama,
    api: api,
    apikey: key,
    description: description,
    device: device
  });

  // Menambahkan session ke file
  const savedSessions = getSessionsFile();
  const sessionIndex = savedSessions.findIndex(sess => sess.id == key);
  if (sessionIndex == -1) {
    savedSessions.push({
      id: key,
      nama: nama,
      api: api,
      apikey: key,
      description: description,
      reanodedy: false,
    });
    setSessionsFile(savedSessions);
  }


}

const init = function (socket) {
  const savedSessions = getSessionsFile();
  if (savedSessions.length > 0) {
    if (socket) {
      savedSessions.forEach((e, i, arr) => {
        arr[i].ready = false;
        const device = sessions.find(sess => sess.id == arr[i].apikey)?.device;
        device.getState().then((result) => {
          if (result == 'CONNECTED') {
            io.emit('ready', { apikey: arr[i].apikey });
            io.emit('message', { apikey: arr[i].apikey, text: 'Whatsapp ready!' });
          }
        })
      });
      socket.emit('init', savedSessions);
    } else {
      savedSessions.forEach(sess => {
        createSession(sess.nama, sess.api, sess.description, sess.id);
      });
    }
  }
}

init();

// Socket IO
io.on('connection', function (socket) {
  init(socket);
  socket.on('create-session', function (data) {
    createSession(data.nama, data.api, data.description, data.apikey);
  });
  socket.on('remove-session', function (data) {
    removeSession(data.apikey);
  });
  socket.on('logout-session', function (data) {
    logoutSession(data.apikey);
  });
  socket.on('get_state', function (data) {
    const device = sessions.find(sess => sess.id == data.apikey)?.device;
    device.getState().then((result) => {
    })
  });
});


//Kirim Pesan text
app.post('/send-text', async (req, res) => {
  //data
  const sender = req.headers.apikey;
  const data = req.body;
  data.number = phoneNumberFormatter(data.number)

  //cek wa pengirim 
  const device = sessions.find(sess => sess.id == sender)?.device;
  if (!device) {
    return res.status(422).json({
      status: false,
      message: `The sender: ${sender} is not found!`
    })
  }

  //cek wa penerima
  if (!data.number.endsWith('@g.us')) {
    const isRegisteredNumber = await device.isRegisteredUser(data.number);
    if (!isRegisteredNumber) {
      return res.status(422).json({
        status: false,
        message: `The number + ${data.number} is not registered`
      });
    }
  }

  // kirim wa 
  device.sendMessage(data.number, data.message).then(response => {
    res.status(200).json({
      status: true,
      response: response
    });
  }).catch(err => {
    res.status(500).json({
      status: false,
      response: err
    });
  });
});

//Kirim Pesan List
app.post('/send-list', async (req, res) => {
  //data
  const sender = req.headers.apikey;
  const data = req.body;
  data.number = phoneNumberFormatter(data.number)

  //cek wa pengirim 
  const device = sessions.find(sess => sess.id == sender)?.device;
  if (!device) {
    return res.status(422).json({
      status: false,
      message: `The sender: ${sender} is not found!`
    })
  }

  //cek wa penerima
  if (!data.number.endsWith('@g.us')) {
    const isRegisteredNumber = await device.isRegisteredUser(data.number);
    if (!isRegisteredNumber) {
      return res.status(422).json({
        status: false,
        message: `The number + ${data.number} is not registered`
      });
    }
  }

  // Membuat list
  const list = new List(
    data.description,
    data.button,
    [
      {
        title: data.list.name,
        rows: data.list.rows,
      },
    ],
    "*" + data.title + "*"
  );

  // kirim wa 
  device.sendMessage(data.number, list).then(response => {
    res.status(200).json({
      status: true,
      response: response
    });
  }).catch(err => {
    res.status(500).json({
      status: false,
      response: err
    });
  });
});

//Kirim Pesan button
app.post('/send-button', async (req, res) => {
  //data
  const sender = req.headers.apikey;
  const data = req.body;
  data.number = phoneNumberFormatter(data.number)

  //cek wa pengirim 
  const device = sessions.find(sess => sess.id == sender)?.device;
  if (!device) {
    return res.status(422).json({
      status: false,
      message: `The sender: ${sender} is not found!`
    })
  }

  //cek wa penerima
  if (!data.number.endsWith('@g.us')) {
    const isRegisteredNumber = await device.isRegisteredUser(data.number);
    if (!isRegisteredNumber) {
      return res.status(422).json({
        status: false,
        message: `The number + ${data.number} is not registered`
      });
    }
  }

  // Membuat Button
  let button = new Buttons(
    data.description,
    data.rows,
    data.title,
    data.footer
  );

  // kirim wa 
  device.sendMessage(data.number, button).then(response => {
    res.status(200).json({
      status: true,
      response: response
    });
  }).catch(err => {
    res.status(500).json({
      status: false,
      response: err
    });
  });
});

//Kirim Pesan media
app.post('/send-media', async (req, res) => {
  //data
  const sender = req.headers.apikey;
  const data = req.body;
  data.number = phoneNumberFormatter(data.number)

  //cek wa pengirim 
  const device = sessions.find(sess => sess.id == sender)?.device;
  if (!device) {
    return res.status(422).json({
      status: false,
      message: `The sender: ${sender} is not found!`
    })
  }

  //cek wa penerima
  if (!data.number.endsWith('@g.us')) {
    const isRegisteredNumber = await device.isRegisteredUser(data.number);
    if (!isRegisteredNumber) {
      return res.status(422).json({
        status: false,
        message: `The number + ${data.number} is not registered`
      });
    }
  }

  // Membuat Base 64
  let mimetype;
  const attachment = await axios.get(data.fileUrl, {
    responseType: 'arraybuffer'
  }).then(response => {
    mimetype = response.headers['content-type'];
    return response.data.toString('base64');
  }).catch(err => {
    res.status(400).json({
      status: false,
      response: 'Bad request. plase check your file url, makesure your file is exist!'
    });
  });

  if (attachment) { // validasi file exist

    //buat media
    const media = new MessageMedia(mimetype, attachment, data.fileName);

    // kirim wa 
    device.sendMessage(data.number, media, { caption: data.caption }).then(response => {
      res.status(200).json({
        status: true,
        response: response
      });
    }).catch(err => {
      res.status(500).json({
        status: false,
        response: err
      });
    });
  }
});

// Kirim QR code
app.post('/send-qr', async (req, res) => {
  //data
  const sender = req.headers.apikey;
  const data = req.body;
  data.number = phoneNumberFormatter(data.number)

  //cek wa pengirim 
  const device = sessions.find(sess => sess.id == sender)?.device;
  if (!device) {
    return res.status(422).json({
      status: false,
      message: `The sender: ${sender} is not found!`
    })
  }

  //cek wa penerima
  if (!data.number.endsWith('@g.us')) {
    const isRegisteredNumber = await device.isRegisteredUser(data.number);
    if (!isRegisteredNumber) {
      return res.status(422).json({
        status: false,
        message: `The number + ${data.number} is not registered`
      });
    }
  }

  // Membuat QR
  if (data.qrValue != undefined) {
    qrcode.toDataURL(data.qrValue, { errorCorrectionLevel: 'L' }, (err, src) => {
      src = src.replace('data:image/png;base64,', '');
      const media = new MessageMedia('image/png', src, data.qrName);
      // kirim wa 
      device.sendMessage(data.number, media, { caption: data.caption }).then(response => {
        res.status(200).json({
          status: true,
          response: response
        });
      }).catch(err => {
        res.status(500).json({
          status: false,
          response: err
        });
      });
    });
  }
  // console.log(qq);
});

app.get('/get-chat-list', async (req, res) => {
  //data
  const sender = req.headers.apikey;
  // const data = req.body;
  // data.number = phoneNumberFormatter(data.number)

  const device = sessions.find(sess => sess.id == sender)?.device;
  if (!device) {
    return res.status(422).json({
      status: false,
      message: `The sender: ${sender} is not found!`
    })
  }

  device.getContacts().then(response => {
    res.status(200).json({
      status: true,
      response: response
    });
  }).catch(err => {
    res.status(500).json({
      status: false,
      response: err
    });
  });
});

app.post('/send-qris', async (req, res) => {
  //data
  const sender = req.headers.apikey;
  const data = req.body;
  data.number = phoneNumberFormatter(data.number)

  //cek wa pengirim 
  const device = sessions.find(sess => sess.id == sender)?.device;
  if (!device) {
    return res.status(422).json({
      status: false,
      message: `The sender: ${sender} is not found!`
    })
  }

  //cek wa penerima
  if (!data.number.endsWith('@g.us')) {
    const isRegisteredNumber = await device.isRegisteredUser(data.number);
    if (!isRegisteredNumber) {
      return res.status(422).json({
        status: false,
        message: `The number + ${data.number} is not registered`
      });
    }
  }
  get_qris_value(data, (error, qris) => {
    if (error) {
      res.status(500).json({
        status: false,
        response: error
      });
    } else {
      // Membuat QR
      if (qris.rawqr != undefined) {
        qrcode.toDataURL(qris.rawqr, { errorCorrectionLevel: 'L' }, (err, src) => {
          src = src.replace('data:image/png;base64,', '');
          const media = new MessageMedia('image/png', src, data.ref);
          // kirim wa 
          device.sendMessage(data.number, media, { caption: data.caption }).then(response => {
            res.status(200).json({
              status: true,
              response: response
            });
          }).catch(err => {
            res.status(500).json({
              status: false,
              response: err
            });
          });
        });
      } else {
        res.status(500).json({
          status: false,
          response: qris
        });
      }
    }
  });
});

function get_qris_value(post, callback) {
  const options = {
    hostname: 'api.paydia.id',
    path: '/qris/generate/',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer Nzc0NmE1NTUwZWUwNzQwOTc1NzI0M2VhNDMyNzdmYzc6ZmZhYjc5NjFmMjI0YmFkNTMxNzMxYzY4MGM2MzNiMDM6MjMwNTE1MDE3MDAwMDAw'
    }
  };

  const reqdata = {
    merchantid: "230515017000000",
    nominal: post.nominal,
    tip: post.tip,
    ref: post.ref,
    callback: post.callback,
    expire: 1440
  };

  const req = https.request(options, res => {
    let data = '';

    res.on('data', chunk => {
      data += chunk;
    });

    res.on('end', () => {
      callback(null, JSON.parse(data));
    });
  });

  req.on('error', error => {
    callback(error);
  });

  req.write(JSON.stringify(reqdata));
  req.end();
}


const Replay = './replay.json';

const Replay_file = function () {
  if (!fs.existsSync(Replay)) {
    try {
      fs.writeFileSync(Replay, JSON.stringify([]));
      console.log('Sessions file created successfully.');
    } catch (err) {
      console.log('Failed to create sessions file: ', err);
    }
  }
}

const getReplay_file = function () {
  return JSON.parse(fs.readFileSync(Replay));
}

app.post('/replay', async (req, res) => {
  //data
  const sender = req.headers.apikey;
  const data = req.body;
  data.number = phoneNumberFormatter(data.number);

  // Menambahkan session ke file
  const savedSessions = getReplay_file();
  // const sessionIndex = savedSessions.findIndex(sess => sess.id == key);
  // if (sessionIndex == -1) {
  console.log(data);
  // savedSessions.push(data);
  // setSessionsFile(savedSessions);
  // }
})

Replay_file();

server.listen(port, function () {
  console.log('App running on *: ' + port);
});
